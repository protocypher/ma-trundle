# Trundle

**Hotel System**

Trundle is a basic, text based, hotel room management application that uses the
`cmd.Cmd` framework. It is a reservation system that lets users reserve,
confirm, cancel, and actuate rooms. Once in the room they can extentend or
checkout. It manages different rooms with different criteria and rates. It can
produce reports for which rooms are in use, available, and on hold.

