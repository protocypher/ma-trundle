from setuptools import setup


setup(
      name="Trundle",
      version="0.1.0",
      packages=["trundle"],
      url="https://bitbucket.org/protocypher/ma-trundle",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A hotel room reservation management application."
)

